au VimEnter *.hpp call s:Check_for_empty()

command! InsertHeaders call s:Make_headers()

function! s:Check_for_empty()
	if line('$') == 1 && getline(1) == ''
		echo "Empty header detected, want to fill it with header guards? [y/n]"
		let input = nr2char(getchar())
		if input == 'y' || input == 'Y'
			call s:Make_headers()
		endif
	endif
endfunction


function! s:Make_headers()
	call append(0, "")
	call append(0, "#define " . toupper(expand('%:r') . "_H_"))
	call append(0, "#ifndef " . toupper(expand('%:r') . "_H_"))
	call append('$', "")
	call append('$', "#endif // " . toupper(expand('%:r') . "_H_"))
	echo "Added header guards"
endfunction



" #ifndef FOO_BAR_BAZ_H_
" #define FOO_BAR_BAZ_H_
" ...
" #endif // FOO_BAR_BAZ_H_
